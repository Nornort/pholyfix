
#define trig A0
#define echo A1

float duree;
float distance;


void setup() {

  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);

  Serial.begin(9600);

}




void loop() {

  // Envoi d'une impulsion de 10 us sur la broche Trig
  digitalWrite(trig, LOW);
  delayMicroseconds(5);
  digitalWrite(trig, HIGH);
  delayMicroseconds(15);
  digitalWrite(trig, LOW);

  // récupération de la durée de l'impulsion de retour lue sur la broche Echo
  duree = pulseIn(echo, HIGH);

  //calcul de la distance à l'objet, en cm
  distance = duree *170 / 1000 ;

  Serial.println(distance);
}
